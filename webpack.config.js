const path                 = require( 'path' );
const glob                 = require( 'glob' );
const cleanWebpackPlugin   = require( 'clean-webpack-plugin' );
const htmlWebpackPlugin    = require( 'html-webpack-plugin' );
const miniCssExtractPlugin = require( 'mini-css-extract-plugin' );

const devMode              = 'development' === process.env.NODE_ENV;

const generateHTMLPlugins  = () => glob.sync( './src/**/*.html' ).map( ( dir ) => {
	const filename = path.basename( dir );
	return new htmlWebpackPlugin({
		filename,
		template: path.join( __dirname, 'src', filename ),
	});
});

const loaders = [
	{
		test: /\.(html)$/,
		use: [
			{
				loader: 'html-loader',
			},
		],
	},
	{
		test: /\.js$/,
		exclude: /node_modules/,
		use: [
			{
				loader: 'babel-loader',
				options: {
					presets: [ '@babel/preset-env' ],
				},
			},
		],
	},
	{
		test: /\.s[c|a]ss$/,
		use: [
			miniCssExtractPlugin.loader,
			{
				loader: 'css-loader',
			},
			{
				loader: 'postcss-loader',
				options: {
					plugins: [
						require( 'autoprefixer' ) ({
							'browsers': [ 'last 4 versions' ],
						}),
					],
				},
			},
			{
				loader: 'sass-loader',
				options: {
					outputStyle: devMode ? 'expanded' : 'compressed',
					precision: 10,
				},
			},
		],
	},
	{
		test: /\.(gif|png|jpe?g|svg)$/i,
		use: [
			{
				loader: 'file-loader',
				options: {
					name: 'img/[name].[ext]',
				}
			},
			devMode ? null : {
				loader: 'image-webpack-loader',
				options: {
					bypassOnDebug: true,
					gifsicle: {
						interlaced: false,
					},
					optipng: {
						optimizationLevel: 7,
					},
					pngquant: {
						quality: '65-90',
						speed: 4,
					},
					mozjpeg: {
						progressive: true,
					},
				},
			},
		].filter(Boolean),
	}
];

const plugins = [
	new cleanWebpackPlugin([ 'dist' ]),
	new miniCssExtractPlugin({
		filename: 'style.css',
	}),
	...generateHTMLPlugins(),
];

module.exports = {
	context: path.join( __dirname, 'src' ),
	entry: [
		path.join( __dirname, 'src', 'js/script.js' ),
		path.join( __dirname, 'src', 'css/style.scss' ),
	],
	output: {
		path: path.join( __dirname, 'dist' ),
		filename: 'script.js',
	},
	mode: devMode ? 'development' : 'production',
	devServer: {
		contentBase: path.join( __dirname, 'dist' ),
		watchContentBase: true,
		open: true,
		port: 80,
		writeToDisk: true,
	},
	module: {
		rules: loaders,
	},
	plugins: plugins,
};
